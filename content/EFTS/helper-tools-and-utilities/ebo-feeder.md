---
title: "EBO Web Service Feeder"
date: 2021-06-21T17:55:54+02:00
draft: true
---


This is not the actual EBO Feeder doc. Duh, I wouldn't post that here, since it's "highly confidential". :smirk:  

I am going to use this markdown file for creating colorful alerts, boxes, sections, buttons, etc.
Let's see...

{{< alert style="info" >}} This is the blue one {{< /alert >}}
{{< alert style="success" >}} This is the green one {{< /alert >}}
{{< alert style="danger" >}} Danger! {{< /alert >}}
{{< alert style="warning" >}} Yellow as warning...okay {{< /alert >}}
{{< alert style="dark" >}} Dark alert {{< /alert >}}
{{< alert style="primary" >}} Primary alert {{< /alert >}}
{{< alert style="secondary" >}} Secondary alert {{< /alert >}}  

Panels:  


{{< panel title="Red panel" style="danger" >}} Some content {{< /panel >}}
{{< panel title="Primary" style="primary" >}} Some content {{< /panel >}}
{{< panel title="Purple" style="secondary" >}} Some content {{< /panel >}}
{{< panel title="Warning" style="warning" >}} Some content {{< /panel >}}
{{< panel title="Info" style="info" >}} Some content {{< /panel >}}
{{< panel title="Success" style="success" >}} Some content {{< /panel >}}
{{< panel title="Simple" style="style" >}} Some content {{< /panel >}}  

Buttons:

{{< button style="primary" link="https://yourwebsite.com" >}} Blue {{< /button >}}
{{< button style="secondary" link="https://yourwebsite.com" >}} Purple {{< /button >}}
{{< button style="warning" link="https://yourwebsite.com" >}} Yellow {{< /button >}}
{{< button style="info" link="https://yourwebsite.com" >}} Teal {{< /button >}}
{{< button style="danger" link="https://yourwebsite.com" >}} Red {{< /button >}}
{{< button style="success" link="https://yourwebsite.com" >}} Green {{< /button >}}

Turns out, I can embed videos too :raised_hands:
{{< vimeo id="163917719" class="my-vimeo-wrapper-class" title="My vimeo video" >}}
