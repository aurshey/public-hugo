---
title: "EFTS"
date: 2021-06-21T14:44:33+02:00
draft: true
---

# Electronic Financial Transaction System (EFTS)

Welcome to the documentation for EFTS! We're glad to have you here!

Over here you will find all the documentation related to the EFTS Client and Server applications, REN Self-Service EMV Kernel, Screen Flows, and various Utilities and Helper Tools that have been developed to support the EFTS.


{{< childpages >}}
