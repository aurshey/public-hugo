---
title: "ATMS"
date: 2021-06-21T14:44:53+02:00
draft: true
---

Welcome to the documentation of the ATM Terminal Management System

## Overview

The ATM Terminal Management System is a web based utility for the monitoring and configuring the ATM machines that are a part of the Euronet network.
As of now a newer (and better) version of the ATMS has been rolled out, which has replaced its predecessor (popularly known as the TMS).


{{< panel title="Note" style="info" >}} Please note that we are no longer supporting the older version. {{< /panel >}}
{{< childpages >}}
